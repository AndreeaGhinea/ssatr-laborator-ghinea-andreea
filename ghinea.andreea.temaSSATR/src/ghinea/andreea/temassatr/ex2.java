
package ghinea.andreea.temassatr;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class ex2 {

    public static void main(String[] args) throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        System.out.print("Enter Integer:");
        int number = Integer.parseInt(br.readLine());
        
        if(number<=9 && number>=1 ){
            if(number==1)
                System.out.print("ONE");
            if(number==2)
                System.out.print("TWO");
            if(number==3)
                System.out.print("THREE");
            if(number==4)
                System.out.print("FOUR");
            if(number==5)
                System.out.print("FIVE");
            if(number==6)
                System.out.print("SIX");
            if(number==7)
                System.out.print("SEVEN");
            if(number==8)
                System.out.print("EIGHT");
            if(number==9)
                System.out.print("NINE");
        }
        else{
            System.out.print("OTHER");
        }
    }
    
}
